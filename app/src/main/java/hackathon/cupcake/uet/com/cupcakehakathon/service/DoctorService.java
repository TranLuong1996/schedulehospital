package hackathon.cupcake.uet.com.cupcakehakathon.service;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import hackathon.cupcake.uet.com.cupcakehakathon.AppController;
import hackathon.cupcake.uet.com.cupcakehakathon.common.object.ListIllness;
import hackathon.cupcake.uet.com.cupcakehakathon.common.utils.Constants;
import hackathon.cupcake.uet.com.cupcakehakathon.common.utils.Globals;

/**
 * Created by NgocThai on 25/02/2017.
 */

public class DoctorService
        extends Service {

    private String TAG = "SERVICE";

    private String URL_GET_ILLNESS = "http://cupcake96uet.hol.es/get_all_simple_illness.php";

    private String TAG_REQ_ILLNESS = "ILLNESS";

    private ListIllness listIllness;
//    private Listener.updateListIllness updateListIllness;

    public class LocalBinder extends Binder {
        public DoctorService getService() {
            return DoctorService.this;
        }
    }


    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public boolean onUnbind(Intent intent) {
        return false;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent != null && intent.getExtras() != null) {
            String action = intent.getStringExtra(Constants.CONTROL_SERVICE);
            switch (action) {
                case Constants.VALUE_GET_LIST_ILLNESS: {
                    checkSimpleIllness(this);
                    break;
                }
            }
        }
        return START_STICKY;
    }

    private void checkSimpleIllness(final Context context) {
        StringRequest strReq =
                new StringRequest(Request.Method.GET, URL_GET_ILLNESS, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        String result = response.toString();
                        Gson gson = new Gson();
                        Log.i("SERVICE", "onResponse: " + result);
                        try {
                            listIllness = gson.fromJson(result, ListIllness.class);
                        } catch (JsonSyntaxException e) {
                            e.printStackTrace();
                        }
                        Log.i(TAG, "onResponse: " + listIllness.getSuccess());
                        if (listIllness.getSuccess() == 1) {
                            Globals.lsIllness = listIllness.getSimpleIllness();
                            Intent i = new Intent();
                            i.setAction(Constants.BROADCAST_GET_LIST_ILLNESS);
                            context.sendBroadcast(i);
//                            updateListIllness.updateRcv(lsIllness.getSimpleIllness());
                        } else {
                            // value is null
                            Intent i = new Intent();
                            i.setAction(Constants.BROADCAST_EMPTY_LIST_ILLNESS);
                            context.sendBroadcast(i);
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        String err = error.toString();
                    }
                });
        AppController.getInstance().addToRequestQueue(strReq, TAG_REQ_ILLNESS);
    }

//    public void setUpdateListIllness(Listener.updateListIllness updateListIllness) {
//        this.updateListIllness = updateListIllness;
//    }
}
