package hackathon.cupcake.uet.com.cupcakehakathon.common.object;

/**
 * Created by NgocThai on 25/02/2017.
 */

public class PatientObject {
    private String name, userName;
    private String gender;
    private int age;
    private int identityNumber;
    private String insuranceCode;
    private String passWord, address;

    public PatientObject(String name,
                         String userName,
                         String gender,
                         int age,
                         int identityNumber,
                         String insuranceCode,
                         String passWord,
                         String address) {
        this.name = name;
        this.userName = userName;
        this.gender = gender;
        this.age = age;
        this.identityNumber = identityNumber;
        this.insuranceCode = insuranceCode;
        this.passWord = passWord;
        this.address = address;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getIdentityNumber() {
        return identityNumber;
    }

    public void setIdentityNumber(int identityNumber) {
        this.identityNumber = identityNumber;
    }

    public String getInsuranceCode() {
        return insuranceCode;
    }

    public void setInsuranceCode(String insuranceCode) {
        this.insuranceCode = insuranceCode;
    }

    public String getPassWord() {
        return passWord;
    }

    public void setPassWord(String passWord) {
        this.passWord = passWord;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
