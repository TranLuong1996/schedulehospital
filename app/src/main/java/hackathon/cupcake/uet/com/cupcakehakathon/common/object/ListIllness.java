package hackathon.cupcake.uet.com.cupcakehakathon.common.object;

import java.util.ArrayList;

/**
 * Created by NgocThai on 26/02/2017.
 */

public class ListIllness {

    private ArrayList<IllnessObject> simpleIllness;
    private Integer success;

    public ArrayList<IllnessObject> getSimpleIllness() {
        return simpleIllness;
    }

    public void setSimpleIllness(ArrayList<IllnessObject> simpleIllness) {
        this.simpleIllness = simpleIllness;
    }

    public Integer getSuccess() {
        return success;
    }

    public void setSuccess(Integer success) {
        this.success = success;
    }

}
