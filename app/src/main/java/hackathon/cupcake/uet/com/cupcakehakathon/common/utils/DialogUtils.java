package hackathon.cupcake.uet.com.cupcakehakathon.common.utils;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;

import hackathon.cupcake.uet.com.cupcakehakathon.R;

/**
 * Created by NgocThai on 04/01/2017.
 */

public class DialogUtils {

    //    public static void showDialogInput(final Context context, String title, String msg, String positive, String negative, final TempObject objTemp,
    //                                       DialogInterface.OnDismissListener dismissListener) {
    //        AlertDialog dialog;
    //        AlertDialog.Builder builder = new AlertDialog.Builder(context, R.style.MyDialogTheme);
    //
    //        LayoutInflater inflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
    //        final View view = inflater.inflate(R.layout.edittext_input, null);
    //
    //        builder.setTitle(title);
    //        builder.setMessage(msg);
    //
    //        final EditText edtInput = (EditText) view.findViewById(R.id.edtInputDialog);
    //        final EditText edtFeedback = (EditText) view.findViewById(R.id.edtInputFeedback);
    //        edtInput.requestFocus();
    //        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
    //        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);
    //        builder.setPositiveButton(positive, new DialogInterface.OnClickListener() {
    //            @Override
    //            public void onClick(DialogInterface dialog, int which) {
    //                String name = edtInput.getText().toString();
    //                String feedback = edtFeedback.getText().toString();
    //                if (!feedback.equals("")) {
    //                    objTemp.setStr1(name);
    //                    objTemp.setStr2(feedback);
    //                    InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
    //                    imm.hideSoftInputFromWindow(edtInput.getWindowToken(), 0);
    //                    dialog.dismiss();
    //                } else {
    //                    Toast.makeText(context, "Feedback is empty", Toast.LENGTH_SHORT).show();
    //                    InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
    //                    imm.hideSoftInputFromWindow(edtInput.getWindowToken(), 0);
    //                }
    //            }
    //        });
    //        builder.setNegativeButton(negative, new DialogInterface.OnClickListener() {
    //            @Override
    //            public void onClick(DialogInterface dialog, int which) {
    //                InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
    //                imm.hideSoftInputFromWindow(edtInput.getWindowToken(), 0);
    //                dialog.dismiss();
    //            }
    //        });
    //        builder.setOnDismissListener(dismissListener);
    //        builder.setView(view);
    //        dialog = builder.create();
    //        dialog.show();
    //    }

    public static void showDialog(Context context,
                                  String title,
                                  String message,
                                  String positive,
                                  String negative,
                                  DialogInterface.OnClickListener clickPositive,
                                  DialogInterface.OnClickListener clickNegative) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context, R.style.MyDialogTheme);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setPositiveButton(positive, clickPositive);
        builder.setNegativeButton(negative, clickNegative);
        AlertDialog dialog = builder.create();
        dialog.setCancelable(false);
        dialog.show();
    }

    public static void showDialog(Context context,
                                  String title,
                                  String msg,
                                  String positive,
                                  DialogInterface.OnClickListener dialogPositive) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context, R.style.MyDialogTheme);
        builder.setTitle(title);
        builder.setMessage(msg);
        builder.setPositiveButton(positive, dialogPositive);
        AlertDialog dialog = builder.create();
        dialog.setCancelable(false);
        dialog.show();
    }
}
