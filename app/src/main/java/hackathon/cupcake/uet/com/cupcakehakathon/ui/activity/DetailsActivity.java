package hackathon.cupcake.uet.com.cupcakehakathon.ui.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import hackathon.cupcake.uet.com.cupcakehakathon.R;

public class DetailsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);
    }
}
