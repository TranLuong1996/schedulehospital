package hackathon.cupcake.uet.com.cupcakehakathon.common.utils;

import android.content.Context;
import android.util.Log;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import hackathon.cupcake.uet.com.cupcakehakathon.common.object.PatientObject;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by NgocThai on 03/12/2016.
 */
public class PostDataUtils {

    private static String USERNAME = "userName";
    private static String PASSWORD = "password";
    private static String AGE = "age";
    private static String GENDER = "gender";
    private static String IDENTITY_NUMBER = "identityNumber";
    private static String INSURANCE_CODE = "insuranceCode";
    private static String ADDRESS = "address";

    private static String URL_REGISTER = "http://cupcake96uet.hol.es/create_login_patient.php";
    private static String URL_LOGIN = "http://cupcake96uet.hol.es/check_username_password.php";

    public static void register(final Context context, final PatientObject patientObject) {
        StringRequest stringRequest =
            new StringRequest(Request.Method.POST, URL_REGISTER, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.i("REGISTER", "onResponse: " + response.toString());
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.i("REGISTER", "onResponse: " + error.toString());
                }
            }) {
                /**
                 * @return
                 */
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put(USERNAME, patientObject.getUserName());
                    params.put(PASSWORD, patientObject.getPassWord());
                    params.put(AGE, patientObject.getAge() + "");
                    params.put(GENDER, patientObject.getGender() + "");
                    params.put(IDENTITY_NUMBER, patientObject.getIdentityNumber() + "");
                    params.put(INSURANCE_CODE, patientObject.getInsuranceCode());
                    params.put(ADDRESS, patientObject.getAddress());

                    return params;
                }

                @Override
                public Priority getPriority() {
                    return Priority.IMMEDIATE;
                }
            };
        RequestQueue requestQueue = Volley.newRequestQueue(context);
        requestQueue.add(stringRequest);
    }

    public static void login(final Context context, final String username, final String password) {
        StringRequest stringRequest =
            new StringRequest(Request.Method.POST, URL_LOGIN, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.i("REGISTER", "onResponse: " + response.toString());
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.i("REGISTER", "onResponse: " + error.toString());
                }
            }) {
                /**
                 * @return
                 */
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put(USERNAME, username);
                    params.put(PASSWORD, password);
                    return params;
                }

                @Override
                public Priority getPriority() {
                    return Priority.IMMEDIATE;
                }
            };
        RequestQueue requestQueue = Volley.newRequestQueue(context);
        requestQueue.add(stringRequest);
    }
}
