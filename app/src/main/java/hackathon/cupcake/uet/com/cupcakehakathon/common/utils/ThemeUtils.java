package hackathon.cupcake.uet.com.cupcakehakathon.common.utils;

import android.annotation.TargetApi;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Window;
import android.view.WindowManager;

/**
 * Created by NgocThai on 05/01/2017.
 */
public class ThemeUtils {

    public static final void changeColorActionbar(AppCompatActivity context,
                                                  String title,
                                                  boolean iconHome,
                                                  String colorActionbar) {
        ActionBar actionBar = context.getSupportActionBar();
        actionBar.setTitle(title);
        actionBar.setDisplayHomeAsUpEnabled(iconHome);
        actionBar.setBackgroundDrawable(new ColorDrawable(Color.parseColor(colorActionbar)));
    }

    /**
     * @param context
     * @param toolbar
     * @param title
     * @param iconHome
     * @param colorToolbar
     */
    public static void changeColorToolbarString(AppCompatActivity context,
                                                Toolbar toolbar,
                                                String title,
                                                boolean iconHome,
                                                String colorToolbar) {
        context.setSupportActionBar(toolbar);
        toolbar.setTitle(title);
        context.getSupportActionBar().setDisplayHomeAsUpEnabled(iconHome);
        toolbar.setBackgroundDrawable(new ColorDrawable(Color.parseColor(colorToolbar)));
    }

    /**
     * @param context
     * @param toolbar
     * @param title
     * @param iconHome
     * @param colorToolbar
     */
    public static void changeColorToolbarResource(AppCompatActivity context,
                                                  Toolbar toolbar,
                                                  String title,
                                                  boolean iconHome,
                                                  int colorToolbar) {
        context.setSupportActionBar(toolbar);
        toolbar.setTitle(title);
        context.getSupportActionBar().setDisplayHomeAsUpEnabled(iconHome);
        toolbar.setBackgroundDrawable(new ColorDrawable(context.getResources()
                                                            .getColor(colorToolbar)));
    }

    /**
     * @param context
     * @param colorWindows
     */
    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public static void changeColorWindows(AppCompatActivity context, String colorWindows) {
        Window window = context.getWindow();
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.setStatusBarColor(Color.parseColor(colorWindows));
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public static void changeColorWindows(AppCompatActivity context, int colorWindows) {
        Window window = context.getWindow();
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.setStatusBarColor(colorWindows);
    }

    /**
     * @param context
     * @param textView
     * @param color
     */
    //    public static void backgroundTextview(Context context, TextView textView, String color) {
    //        ShapeDrawable footerBackground = new ShapeDrawable();
    //        int dimenColor = R.dimen.corners_color;
    //        float[] radii = new float[8];
    //        radii[0] = context.getResources().getDimension(dimenColor);
    //        radii[1] = context.getResources().getDimension(dimenColor);
    //        radii[2] = context.getResources().getDimension(dimenColor);
    //        radii[3] = context.getResources().getDimension(dimenColor);
    //        radii[4] = context.getResources().getDimension(dimenColor);
    //        radii[5] = context.getResources().getDimension(dimenColor);
    //        radii[6] = context.getResources().getDimension(dimenColor);
    //        radii[7] = context.getResources().getDimension(dimenColor);
    //        footerBackground.setShape(new RoundRectShape(radii, null, null));
    //        footerBackground.getPaint().setColor(Color.parseColor(color));
    //        textView.setBackgroundDrawable(footerBackground);
    //    }
    //
    //    public static void backgroundButton(Context context, Button button, String color) {
    //        ShapeDrawable footerBackground = new ShapeDrawable();
    //        int dimenColor = R.dimen.corners_button;
    //        float[] radii = new float[8];
    //        radii[0] = context.getResources().getDimension(dimenColor);
    //        radii[1] = context.getResources().getDimension(dimenColor);
    //        radii[2] = context.getResources().getDimension(dimenColor);
    //        radii[3] = context.getResources().getDimension(dimenColor);
    //        radii[4] = context.getResources().getDimension(dimenColor);
    //        radii[5] = context.getResources().getDimension(dimenColor);
    //        radii[6] = context.getResources().getDimension(dimenColor);
    //        radii[7] = context.getResources().getDimension(dimenColor);
    //        footerBackground.setShape(new RoundRectShape(radii, null, null));
    //        footerBackground.getPaint().setColor(Color.parseColor(color));
    //        button.setBackgroundDrawable(footerBackground);
    //    }
}
