package hackathon.cupcake.uet.com.cupcakehakathon.common.listener;

import java.util.ArrayList;

import hackathon.cupcake.uet.com.cupcakehakathon.common.object.IllnessObject;

/**
 * Created by NgocThai on 25/02/2017.
 */

public class Listener {

    public interface listenIllness {
        void onClick(int id);
    }

    public interface updateListIllness {
        void updateRcv(ArrayList<IllnessObject> listIllness);
    }
}
