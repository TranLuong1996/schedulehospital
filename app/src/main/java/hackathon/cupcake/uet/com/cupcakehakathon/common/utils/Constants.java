package hackathon.cupcake.uet.com.cupcakehakathon.common.utils;

/**
 * Created by NgocThai on 25/02/2017.
 */

public class Constants {

    public static final String CONTROL_SERVICE = "CONTROL_SERVICE";

    public static final String VALUE_GET_LIST_ILLNESS = "GET_LIST_ILLNESS";

    public static final String BROADCAST_EMPTY_LIST_ILLNESS = "LIST_ILLNESS_EMPTY";
    public static final String BROADCAST_GET_LIST_ILLNESS = "LIST_ILLNESS_SHOW";


}
