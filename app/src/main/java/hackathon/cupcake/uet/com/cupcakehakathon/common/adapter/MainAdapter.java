package hackathon.cupcake.uet.com.cupcakehakathon.common.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.andexert.library.RippleView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import hackathon.cupcake.uet.com.cupcakehakathon.R;
import hackathon.cupcake.uet.com.cupcakehakathon.common.library.ColorGenerator;
import hackathon.cupcake.uet.com.cupcakehakathon.common.library.TextDrawable;
import hackathon.cupcake.uet.com.cupcakehakathon.common.listener.Listener;
import hackathon.cupcake.uet.com.cupcakehakathon.common.object.IllnessObject;

/**
 * Created by NgocThai on 25/02/2017.
 */

public class MainAdapter extends RecyclerView.Adapter<MainAdapter.ViewHolder> {

    private ArrayList<IllnessObject> lsIllness;
    private Context context;
    private Listener.listenIllness listenIllness;

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.rvIllness)
        RippleView rvIllness;
        @BindView(R.id.txtIllness)
        TextView txtIllness;
        @BindView(R.id.txtDescIllness)
        TextView txtDescIllness;
        @BindView(R.id.imgLetterIllness)
        ImageView imgLetterIllness;

        public ViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
        }
    }

    public MainAdapter(ArrayList<IllnessObject> ls, Context context) {
        this.lsIllness = ls;
        this.context = context;
    }

    @Override
    public MainAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_illness, parent, false);
        MainAdapter.ViewHolder viewHolder = new MainAdapter.ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(MainAdapter.ViewHolder holder, final int position) {
        final IllnessObject illnessObject = lsIllness.get(position);
        holder.txtIllness.setText(illnessObject.getName());
        holder.txtDescIllness.setText(illnessObject.getDescription());
        // get image from string first letter
        String firstLetter = String.valueOf(illnessObject.getName().charAt(0));
        ColorGenerator generator = ColorGenerator.MATERIAL_COLOR;
        int color = generator.getRandomColor();
        TextDrawable drawable = TextDrawable.builder()
                .buildRound(firstLetter, color);
        holder.imgLetterIllness.setImageDrawable(drawable);
        holder.rvIllness.setRippleDuration(250);
        holder.rvIllness.setOnRippleCompleteListener(new RippleView.OnRippleCompleteListener() {
            @Override
            public void onComplete(RippleView rippleView) {
                listenIllness.onClick(position);
            }
        });

    }

    @Override
    public int getItemCount() {
        return lsIllness.size();
    }

    public void setListenIllness(Listener.listenIllness listenIllness) {
        this.listenIllness = listenIllness;
    }
}
