package hackathon.cupcake.uet.com.cupcakehakathon;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import hackathon.cupcake.uet.com.cupcakehakathon.common.RecycleView.RecycleUtils;
import hackathon.cupcake.uet.com.cupcakehakathon.common.adapter.MainAdapter;
import hackathon.cupcake.uet.com.cupcakehakathon.common.listener.Listener;
import hackathon.cupcake.uet.com.cupcakehakathon.common.object.IllnessObject;
import hackathon.cupcake.uet.com.cupcakehakathon.common.object.ListIllness;
import hackathon.cupcake.uet.com.cupcakehakathon.common.utils.Constants;
import hackathon.cupcake.uet.com.cupcakehakathon.common.utils.Globals;
import hackathon.cupcake.uet.com.cupcakehakathon.common.utils.PostDataUtils;
import hackathon.cupcake.uet.com.cupcakehakathon.service.DoctorService;
import hackathon.cupcake.uet.com.cupcakehakathon.ui.activity.BaseActivity;

public class MainActivity extends BaseActivity {

    @BindView(R.id.txtRegister)
    TextView txtRegister;
    @BindView(R.id.rcvIllness)
    RecyclerView recyclerView;

    private MainAdapter adapter;

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_main;
    }

    @Override
    protected void initVariables(Bundle saveInstanceState) {

    }

    @Override
    protected void initData(Bundle saveInstanceState) {
        ButterKnife.bind(this);
        Intent i = new Intent(MainActivity.this, DoctorService.class);
        i.putExtra(Constants.CONTROL_SERVICE, Constants.VALUE_GET_LIST_ILLNESS);
        startService(i);
    }


    @OnClick(R.id.txtRegister)
    public void clickTest() {
        PostDataUtils.login(MainActivity.this, "trinhquocdat", "arsenan1");
        //PostDataUtils.register(MainActivity.this,
        //                       new PatientObject("Thai NGoc",
        //                                         "thaihn2",
        //                                         "1",
        //                                         21,
        //                                         2222,
        //                                         "hsafdjsadf",
        //                                         "aaaaaa",
        //                                         "ha noi"));
    }

//    @Override
//    public void updateRcv(ArrayList<IllnessObject> listIllness) {
//        adapter = new MainAdapter(listIllness, this);
//        RecycleUtils.showListRcv(recyclerView, adapter, new Listener.listenIllness() {
//            @Override
//            public void onClick(int id) {
//                Toast.makeText(MainActivity.this, "position " + id, Toast.LENGTH_SHORT).show();
//            }
//        }, this);
//    }

//    public ServiceConnection mConnection = new ServiceConnection() {
//        @Override
//        public void onServiceConnected(ComponentName className,
//                                       IBinder service) {
//            DoctorService.LocalBinder binder = (DoctorService.LocalBinder) service;
//            MainActivity.this.mService = binder.getService();
//            MainActivity.this.mService.setUpdateListIllness(MainActivity.this);
//            isBound = true;
//        }
//
//        @Override
//        public void onServiceDisconnected(ComponentName arg0) {
//            isBound = false;
//        }
//    };

    BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            switch (action) {
                case Constants.BROADCAST_EMPTY_LIST_ILLNESS: {
                    Toast.makeText(context, "EMPTY LIST", Toast.LENGTH_SHORT).show();
                    break;
                }
                case Constants.BROADCAST_GET_LIST_ILLNESS: {
                    adapter = new MainAdapter(Globals.lsIllness, MainActivity.this);
                    RecycleUtils.showListRcv(recyclerView, adapter, new Listener.listenIllness() {
                        @Override
                        public void onClick(int id) {
                            Toast.makeText(MainActivity.this, "position " + id, Toast.LENGTH_SHORT).show();
                        }
                    }, MainActivity.this);
                    break;
                }
            }
        }
    };

    @Override
    protected void onStart() {
        super.onStart();
        // broad cast
        registerReceiver(broadcastReceiver, new IntentFilter(Constants.BROADCAST_EMPTY_LIST_ILLNESS));
        registerReceiver(broadcastReceiver, new IntentFilter(Constants.BROADCAST_GET_LIST_ILLNESS));
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (broadcastReceiver != null) {
            unregisterReceiver(broadcastReceiver);
        }

    }
}
