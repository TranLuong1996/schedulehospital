package hackathon.cupcake.uet.com.cupcakehakathon.common.RecycleView;

import android.content.Context;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import hackathon.cupcake.uet.com.cupcakehakathon.common.adapter.MainAdapter;
import hackathon.cupcake.uet.com.cupcakehakathon.common.listener.Listener;

/**
 * Created by NgocThai on 25/02/2017.
 */

public class RecycleUtils {

    public static final void showListRcv(RecyclerView mRecycleView, MainAdapter adapter, Listener.listenIllness listenIllness, Context context) {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
        mRecycleView.setHasFixedSize(true);
        mRecycleView.setLayoutManager(linearLayoutManager);
        mRecycleView.setItemAnimator(new DefaultItemAnimator());
        mRecycleView.setAdapter(adapter);
        adapter.setListenIllness(listenIllness);
        adapter.notifyDataSetChanged();
    }

}
